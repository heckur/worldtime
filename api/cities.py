import pytz
import sqlite3
from datetime import datetime
from string import punctuation

# Characters that we don't want to take into account when searching the db for city names
EXCLUDED_CHARS = "{} ".format(punctuation)

def db_conn():
  return sqlite3.connect('db/cities.db')

def get_hours(timezone):
  """
    Returns a dictionary of hours for the timezone starting at the current hour
    resetting to 0 after 23hrs
  """
  utctime = datetime.utcnow()
  tz = pytz.timezone(timezone)
  localtime = pytz.utc.localize(utctime).astimezone(tz)
  hour = localtime.hour
  hours = {}
  index = 0
  while index < 23:
    if hour == 24:
      hour = 0
    hours[index] = hour
    hour += 1
    index += 1
  return hours

def search(abc):
  """
    Search the db for cities that contain the pattern abc in their name: case insensitive
  """
  conn = db_conn()
  c = conn.cursor()
  pattern = "".join([x for x in abc if x not in EXCLUDED_CHARS])
  resultset = c.execute("SELECT city.id, city.name, country.name, city.timezone FROM city LEFT JOIN country on city.country_id = country.id WHERE city.name LIKE :pattern ORDER BY city.name", {"pattern": "%{}%".format(pattern)})
  return [{"id": r[0], "name": r[1], "country": r[2], "timezone": r[3], "hours": get_hours(r[3])} for r in resultset]
