Vue.component("v-select", VueSelect.VueSelect);

var app = new Vue({
  el: "#city_times",
  data: {
    cities: {},
    pattern: "",
    options: [],
    selected: null
  },
  watch: {
    selected: function(new_selected, old_selected){
      this.cities[new_selected.id] = {name: new_selected.name, country: new_selected.country, timezone: new_selected.timezone, hours: new_selected.hours}
    }
  },
  methods: {
    search: function search(search, loading){
      loading(true);
      this.get_cities(loading, search, this);
    },
    get_cities: _.debounce(function(loading, search, vm) {
      var params = {
        abc: escape(search)
      };
      axios.get("/api/cities", {params: params}).then(function(response){
        return vm.options = response.data;
      });
      loading(false);
    },
    400)
  }
});
