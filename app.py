import connexion
from flask import render_template

PORT=7777

# Using the connexion module for easy API creation
app = connexion.FlaskApp(__name__, port=PORT, specification_dir="yaml/")
app.add_api("api.yaml", resolver=connexion.RestyResolver("api"))

@app.route("/")
def home():
  # Single page application
  return render_template("home.html")

if __name__ == "__main__":
  app.run()
